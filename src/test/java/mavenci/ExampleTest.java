package mavenci;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ExampleTest {
	@Test
	public void sayHelloTest() {
		assertEquals("hello world", Example.sayHello());
	}
}
